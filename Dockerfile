FROM redis:6.2.6

EXPOSE 6379

CMD ["redis-server"]
